#!/bin/bash

#
# This script is for creating new snapshots of the MergeTB repository
# Under normal circumstances this should only be run in use-cases where the gpg key was updated etc.,
# as the ci-cd jobs will run its sibling add-deb.sh (which this script was cribbed from)
#

RED="\e[31m"
BLUE="\e[34m"
GREEN="\e[32m"
CYAN="\e[36m"
NORMAL="\e[39m"
RESET="\e[0m"

red() { 
  echo -e "$RED$1$RESET" 
}
blue() { 
  echo -e "$BLUE$1$RESET" 
}
green() { 
  echo -e "$GREEN$1$RESET" 
}
cyan() { 
  echo -e "$CYAN$1$RESET" 
}

RELEASE=${RELEASE:-buster}
REPO=${REPO:-debian-$RELEASE}
repo=$REPO

apikey="apikey: $PKG_API_KEY"
json="Content-Type: application/json"
url="https://pkg-api.mergetb.net"

green "adding `basename $file` to repository"

# unpublish delete current snapshot
blue "unpublishing existing snapshot"
curl -H "$apikey" -X DELETE "$url/publish/filesystem:mergetb:debian/$RELEASE"; echo ""
blue "deleting existing snapshot"
curl -H "$apikey" -X DELETE "$url/snapshots/$repo"; echo ""

# create and publish new snapshot
read -r -d '' data << EOF
{
  "Name": "$repo"
}
EOF

blue "creating new snapshot"
curl \
  -H "$apikey" \
  -H "$json" \
  --data "$data" \
  -X POST "$url/repos/$repo/snapshots"; echo ""

# publish new snapshot
read -r -d '' data << EOF 
{
  "SourceKind": "snapshot", 
  "Sources": [{"Name": "$repo"}], 
  "Distribution": "$RELEASE",
  "ForceOverwrite": true
}
EOF

blue "publishing new snapshot"
curl \
  -H "$apikey" \
  -H "$json" \
  --data "$data" \
  -X POST "$url/publish/filesystem:mergetb:debian"; echo ""

green "done!"

