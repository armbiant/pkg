#!/bin/bash

# This script is for adding a debian package to the MergeTB repository from the
# MergeTB CI server.

RED="\e[31m"
BLUE="\e[34m"
GREEN="\e[32m"
CYAN="\e[36m"
NORMAL="\e[39m"
RESET="\e[0m"

red() { 
  echo -e "$RED$1$RESET" 
}
blue() { 
  echo -e "$BLUE$1$RESET" 
}
green() { 
  echo -e "$GREEN$1$RESET" 
}
cyan() { 
  echo -e "$CYAN$1$RESET" 
}

RELEASE=${RELEASE:-buster}
REPO=${REPO:-debian-$RELEASE}
usage="[RELEASE = buster|bullseye|sid | zelda|link|kass] add-deb.sh <file.deb>"

if [[ $# -ne 1 ]]; then
  echo $usage
  exit 1
fi
file=$1

repo=$REPO

apikey="apikey: $PKG_API_KEY"
json="Content-Type: application/json"
url="https://pkg-api.mergetb.net"

green "adding `basename $file` to repository"

# add package to repo
blue "uploading deb"
curl -H "$apikey" -X POST -F file=@$file "$url/files/uploads"; echo ""
blue "adding deb to repo"
curl -H "$apikey" -X POST "$url/repos/$repo/file/uploads/`basename $file`?forceReplace=1"; echo ""

# unpublish delete current snapshot
blue "unpublishing existing snapshot"
curl -H "$apikey" -X DELETE "$url/publish/filesystem:mergetb:debian/$RELEASE"; echo ""
blue "deleting existing snapshot"
curl -H "$apikey" -X DELETE "$url/snapshots/$repo"; echo ""

# create and publish new snapshot based on updated repo
read -r -d '' data << EOF
{
  "Name": "$repo"
}
EOF

blue "creating new snapshot"
curl \
  -H "$apikey" \
  -H "$json" \
  --data "$data" \
  -X POST "$url/repos/$repo/snapshots"; echo ""

# publish new snapshot
read -r -d '' data << EOF 
{
  "SourceKind": "snapshot", 
  "Sources": [{"Name": "$repo"}], 
  "Distribution": "$RELEASE",
  "ForceOverwrite": true
}
EOF

blue "publishing new snapshot"
curl \
  -H "$apikey" \
  -H "$json" \
  --data "$data" \
  -X POST "$url/publish/filesystem:mergetb:debian"; echo ""

green "done!"
